﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OptimisticConcurrencyWebRoundtrip.WebApplication.Models.DataAccess;

namespace OptimisticConcurrencyWebRoundtrip.WebApplication.Controllers
{
	public class PostingController : Controller
	{
		private readonly BloggingContext _context;

		public PostingController(BloggingContext context)
		{
			_context = context;
		}

		// GET: Posting
		//public async Task<IActionResult> Index()
		//{
		//	var bloggingContext = _context.Posts.Include(p => p.Blog);
		//	return View(await bloggingContext.ToListAsync());
		//}

		// GET: Posting
		public async Task<IActionResult> Index(int? id)
		{
			if (!id.HasValue)
			{
				var allPostings = _context.Posts.Include(p => p.Blog);
				return View(await allPostings.ToListAsync());
			}

			var blogPostings = _context.Posts.Include(p => p.Blog).Where(p => p.BlogId == id);
			return View(await blogPostings.ToListAsync());
		}

		// GET: Posting/Details/5
		public async Task<IActionResult> Details(int? id)
		{
			if (id == null) return NotFound();

			var post = await _context.Posts
				.Include(p => p.Blog)
				.FirstOrDefaultAsync(m => m.Id == id);
			if (post == null) return NotFound();

			return View(post);
		}

		// GET: Posting/Create
		public IActionResult Create()
		{
			ViewData["BlogId"] = new SelectList(_context.Blogs, "Id", "Url");
			return View();
		}

		// POST: Posting/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("Id,Title,Content,BlogId,Version")]
			Post post)
		{
			if (ModelState.IsValid)
			{
				_context.Add(post);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}

			ViewData["BlogId"] = new SelectList(_context.Blogs, "Id", "Url", post.BlogId);
			return View(post);
		}

		// GET: Posting/Edit/5
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null) return NotFound();

			var post = await _context.Posts.FindAsync(id);
			if (post == null) return NotFound();
			ViewData["BlogId"] = new SelectList(_context.Blogs, "Id", "Url", post.BlogId);
			return View(post);
		}

		// POST: Posting/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Content,BlogId,Version")]
			Post post)
		{
			if (id != post.Id) return NotFound();

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(post);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!PostExists(post.Id))
						return NotFound();
					throw;
				}

				return RedirectToAction(nameof(Index));
			}

			ViewData["BlogId"] = new SelectList(_context.Blogs, "Id", "Url", post.BlogId);
			return View(post);
		}

		// GET: Posting/Delete/5
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null) return NotFound();

			var post = await _context.Posts
				.Include(p => p.Blog)
				.FirstOrDefaultAsync(m => m.Id == id);
			if (post == null) return NotFound();

			return View(post);
		}

		// POST: Posting/Delete/5
		[HttpPost]
		[ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(Post post)
		{
			
			_context.Remove(post);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}

		private bool PostExists(int id)
		{
			return _context.Posts.Any(e => e.Id == id);
		}
	}
}