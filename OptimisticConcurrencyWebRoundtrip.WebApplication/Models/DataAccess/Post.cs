﻿using System.ComponentModel.DataAnnotations;

namespace OptimisticConcurrencyWebRoundtrip.WebApplication.Models.DataAccess
{
	public class Post
	{
		[Key] public int Id { get; set; }

		[Required] [MaxLength(50)] public string Title { get; set; }

		[Required] public string Content { get; set; }

		public int BlogId { get; set; }

		[Timestamp] public byte[] Version { get; set; }

		public Blog Blog { get; set; }
	}
}