﻿using Microsoft.EntityFrameworkCore;

namespace OptimisticConcurrencyWebRoundtrip.WebApplication.Models.DataAccess
{
	public class BloggingContext : DbContext
	{
		public DbSet<Blog> Blogs { get; set; }
		public DbSet<Post> Posts { get; set; }


		public BloggingContext(DbContextOptions options) : base(options)
		{
			
		}

		protected override void OnConfiguring(DbContextOptionsBuilder options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Blog>().HasData(new Blog {Id = 1, Url = "http://sample.com"});
			modelBuilder.Entity<Post>().HasData(
				new Post {BlogId = 1, Id = 1, Title = "First post", Content = "Test 1"});
			modelBuilder.Entity<Post>().HasData(
				new {BlogId = 1, Id = 2, Title = "Second post", Content = "Test 2"});
		}
	}
}