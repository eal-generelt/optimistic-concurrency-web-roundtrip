﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OptimisticConcurrencyWebRoundtrip.WebApplication.Models.DataAccess
{
	public class Blog
	{
		public int Id { get; set; }

		[Required] public string Url { get; set; }

		[Timestamp] public byte[] Version { get; set; }

		public ICollection<Post> Posts { get; set; }
	}
}